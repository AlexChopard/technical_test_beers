
/**** VUE ****/

// View for beer table
var tableBeers = new Vue({
	el: '#table_beers',
	data: {
		headers: [ // Header of table
					{text: 'Name', value: 'nameDisplay', align: 'left'},
					{text: 'Abv (%)', value: 'abv', align: 'center'},
					{text: 'Ibu', value: 'ibu', align: 'center'},
					{text: 'Is Organic', value: 'isOrganic', align: 'center'},
					{text: 'status', value: 'statusDisplay', align: 'center'},
				 ],
		items: [],
		pagination: { rowsPerPage: 10 },
		pagination_total_visible: 11,
		organic: false
	},
	computed: {
		// Method to calculate the number of pages
		numberOfPage(){
			if( this.pagination.rowsPerPage != null)
				return Math.ceil(this.items.length / this.pagination.rowsPerPage);
			return -1;
		}
	},
	watch: {
		organic: function(val){
			loadData(1, 1);
		}
	}
})




/**** FUNCTION ****/


function loadData(page, styleId){

	// If it's the first load, init data
	if(page == 1){
		tableBeers.items = [];
		tableBeers.pagination.page = 1;
	}

	var data = {
		p: page,
		styleId : styleId,
		isOrganic: (tableBeers.organic) ? 'Y' : 'N',
	}

	$.ajax({
		'type': 'get',
		'url': 'http://api.brewerydb.com/v2/beers?key=6f0d63177f30a4668ce9c170ea9ffe1d',
		'contentType' : "application/json; charset=utf-8",
		'dataType' : 'json',
		'async': true,
		'data': data
	}).done(function(beers){


		// put beers in table items
		beers.data.forEach(function(beer){
			if(!beer.ibu)
				beer.ibu = '-';

			if(!beer.abv)
				beer.abv = '-';

			// Set image 
			if(beer.status == 'verified')
				beer.status = '<img id="image" src="img/validate.png"/>';
			else
				beer.status = '<img id="image" src="img/invalidate.png"/>';

			if(beer.isOrganic == 'Y')
				beer.isOrganic = '<img id="image" src="img/validate.png"/>';
			else
				beer.isOrganic = '<img id="image" src="img/invalidate.png"/>';

			tableBeers.items.push(beer);
		})

		// If it's not the last page, load the next page.
		if(page != beers.numberOfPages)
			loadData(page + 1, styleId);

	}).fail(function(error){
		console.log(error);
	});
}





